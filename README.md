### Technology Assessment

## About App Internal
##### Language used: kotlin,
##### Networking: Retrofit,
##### Android Jetpack: Androidx, RecycleView, Fragment.

## Steps to Run the application
##### 1) Download the application from github or clone it from cammand line by using this command (git clone https://omprakash8080@bitbucket.org/omprakash8080/technicalassessment_13april_bitbucket.git)
##### 2) Import the application into android studio and perform the gradle sync.
##### 3) Choose the Android emulator 
##### 4) Run the application.


